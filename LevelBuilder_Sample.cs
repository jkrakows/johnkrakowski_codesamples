﻿/*
 * AssembleMapChunks() is a recursive function that is called after all of 
 * a level's chunks have been randomly determined. Once determined, the 
 * chunks will be positioned, rotated, and connected by this function. 
 * The algorithm will also detect adjacent, but unconnected chunks and 
 * create cyclic connections when necessary. If a rotation is required, the 
 * chunk's forward and up vectors are positioned to represent a direction
 * where the anchor point rotation is the identity. Next, the anchor 
 * point's destination rotation will be created and applied to the chunk's 
 * forward and up vectors. A rotation is now created with the forward and 
 * up vectors and applied to the chunk.
 */


/// <summary>
/// Recursively assemble all available map chunks. Enable appropriate anchor nodes to
/// connect this chunk to the cluster and to connect this chunk to future chunks.
/// </summary>    
private void AssembleMapChunks(
    List<GameObject> availableMapChunksList,
    PositionNode[,,] positionNodeArray,
    PositionNode     firstNode,
    int              previousAllowedAdjacentCount)
{
    if (availableMapChunksList.Count == 0)
        { return; }

    //Randomly determine which chunk should be assembled next.
    GameObject randomChunk = availableMapChunksList[UnityEngine.Random.Range(0, availableMapChunksList.Count)];
    availableMapChunksList.Remove(randomChunk);

    //Obtain a random position node and assign randomChunk to it.
    PositionNode previousNode;
    PositionNode availablePositionNode = firstNode.GetRandomAvailablePositionNode(out previousNode);
    int          allowedAdjacentCount  = previousAllowedAdjacentCount + 1;

    availablePositionNode.CurMapChunk = randomChunk.GetComponent<MapChunkController>();
    availablePositionNode.InitAllowedAdjacentList(allowedAdjacentCount, previousNode);

    //Handle Anchor Point
    AnchorNode previousAnchorNode  = previousNode.GetAnchorNodeByDirection(previousNode.GetLocalDirectionToNode(availablePositionNode));
    AnchorNode availableAnchorNode = availablePositionNode.GetRandomAnchorNodeByType(previousAnchorNode.anchorType);

    if (previousAnchorNode == null || availableAnchorNode == null)
    {
        HasLevelGenerationError = true;
        return;
    }

    //Enable Anchor Points
    previousAnchorNode.anchorPoint.SetActive(true);
    availableAnchorNode.anchorPoint.SetActive(true);

    if (previousAnchorNode.anchorPoint != null && availableAnchorNode.anchorPoint != null)
    {
        //Perform Quaternion math if necessary.
        if (availableAnchorNode.anchorPoint.transform.forward != -previousAnchorNode.anchorPoint.transform.forward ||
            availableAnchorNode.anchorPoint.transform.up      !=  previousAnchorNode.anchorPoint.transform.up)
        {
            Quaternion anchorPointInverse = Quaternion.Inverse(availableAnchorNode.anchorPoint.transform.rotation);

            //Point CurMapChunk forward and Up in such a direction that anchorPoint.transform.rotation == Quaternion.Identity
            //REMEMBER: AnchorPoints are children of MapChunks
            Vector3 forwardDirection = anchorPointInverse * availablePositionNode.CurMapChunk.transform.forward;
            Vector3 upDirection      = anchorPointInverse * availablePositionNode.CurMapChunk.transform.up;

            //Apply lookRotation, from identity to destination
            Quaternion lookRotation = Quaternion.LookRotation(
                -previousAnchorNode.anchorPoint.transform.forward, 
                 previousAnchorNode.anchorPoint.transform.up);

            forwardDirection = lookRotation * forwardDirection;
            upDirection      = lookRotation * upDirection;

            //Determine new rotation from forward and up vectors
            Quaternion curMapChunkRotation = Quaternion.LookRotation(forwardDirection, upDirection);

            //Apply new rotation
            availablePositionNode.CurMapChunk.transform.rotation = curMapChunkRotation;
        }

        //Apply position
        availablePositionNode.CurMapChunk.transform.position =
            previousNode.CurMapChunk.transform.position + 
            (previousAnchorNode.anchorPoint.transform.forward * GAMECONSTANTS.MAPCHUNK_CUBE_LENGTH);

        //Enable appropriate anchor points to connect this chunk to the existing cluster and allow next chunk to be connected to this one.
        EnableAnchorEnvObjByTag(previousAnchorNode.anchorPoint,  GAMECONSTANTS.TAG_FEMALEANCHORPOINT);
        EnableAnchorEnvObjByTag(availableAnchorNode.anchorPoint, GAMECONSTANTS.TAG_MALEANCHORPOINT);
    }
    else
        { Debug.Log("Error, previousAnchorNode.anchorPoint or availableAnchorNode.anchorPoint is null, skipping to next chunk"); }

    //Handle Cyclic Anchor Points
    //If any number of nodes are arranged in such a way that creates a cycle, this function will ensure that the player can traverse the cycle.
    availablePositionNode.HandleCyclicAnchorPoints();

    //Recurse to the next map chunk.
    AssembleMapChunks(availableMapChunksList, positionNodeArray, firstNode, allowedAdjacentCount);
}

