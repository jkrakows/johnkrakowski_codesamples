﻿/*
 * Each object that exists in the world is a child of 
 * EnvironmentObject, which, has a collision dictionary field. 
 * When a collision is detected the appropriate trigger function 
 * is called, in this example, OnTriggerEnter(). OnTriggerEnter() 
 * will first verify that a behavior exists and then call the 
 * ICollisionBehavior interface. TeleportOnCollision is one of 
 * many classes that implement ICollisionBehavior.
 */


// Implement this interface to create additional collision behaviors.
// This interface supports all physics interactions (OnEnter, OnStay, OnExit).
public interface ICollisionBehavior
{
    void Collision(GameObject self, GameObject other);
}

// Use this behavior in a situation where multiple behaviors are required.
public class MultipleCollisionBehaviors : ICollisionBehavior
{
    private ICollisionBehavior[] list;

    public MultipleCollisionBehaviors(params ICollisionBehavior[] list)
    {
        this.list = list;
    }

    public void Collision(GameObject self, GameObject other)
    {
        foreach (ICollisionBehavior behavior in list)
        {
            behavior.Collision(self, other);
        }
    }
}

// Sets the position and rotation of the colliding object to that of the waypoint object. 
// If nextRoomLayer is given, then PlayerCamera will hide the appropriate room layers from view.
public class TeleportOnCollision : ICollisionBehavior
{
    private GameObject waypointObject;
    private bool isDifferentRoom;
    private RoomLayer nextRoomLayer;

    public TeleportOnCollision(GameObject waypointObject)
    {
        this.waypointObject = waypointObject;
        isDifferentRoom     = false;
    }

    public TeleportOnCollision(GameObject waypointObject, RoomLayer nextRoomLayer)
    {
        this.waypointObject = waypointObject;
        isDifferentRoom     = true;
        this.nextRoomLayer  = nextRoomLayer;
    }

    public void Collision(GameObject self, GameObject other)
    {
        //If other is a PlayerController and is moving, then stop moving.
        PlayerController pc = other.GetComponent<PlayerController>();
        if (pc != null && pc.mover.IsMoverInterpolating)
            { pc.mover.StopInterpolation(); }
        
        other.transform.position = waypointObject.transform.position;
        other.transform.rotation = waypointObject.transform.rotation;

        //Toggle appropriate cullingMask layers
        if (isDifferentRoom)
        {
            PlayerCamera.instance.TeleportCamera();
            foreach (string roomLayer in GAMECONSTANTS.ROOMLAYERARRAY)
            {
                PlayerCamera.instance.HideLayer(roomLayer);
            }

            string roomLayerString = TOOLS.GetRoomLayerString(nextRoomLayer);

            PlayerCamera.instance.ShowLayer(roomLayerString);
            PlayerCamera.instance.CurRoomLayerName = roomLayerString;
            GUIManager.instance.ShowWorldCanvas(roomLayerString);
        }

        //If the teleporter has a trigger sound then play it.
        EnvironmentObject teleportObj = self.GetComponent<EnvironmentObject>();
        if (teleportObj != null && teleportObj.triggerSound != null)
            { teleportObj.PlayEnvObjSound(teleportObj.triggerSound); }
    }
}


/*
******************************************************************
  NOTE: OnTriggerEnter(Collider other) function is located in the 
  EnvironmentObject class, NOT inside ICollisionBehavior.
******************************************************************
*/

// Handles a physics collision of type OnTriggerEnter.
// Behavior will only trigger if collidee and collider have different tags
// and if collisionDictionary contains a behavior for collider's layer.
protected virtual void OnTriggerEnter(Collider other)
{
    string layerName = LayerMask.LayerToName(other.gameObject.layer);

    if (other.attachedRigidbody == null)
        { return; }

    if (!other.attachedRigidbody.CompareTag(gameObjectTag) && collisionDictionary[GAMECONSTANTS.TRIGGERTYPE_ENTER].ContainsKey(layerName))
    {
        collisionDictionary[GAMECONSTANTS.TRIGGERTYPE_ENTER][layerName].Collision(this.gameObject, other.attachedRigidbody.gameObject);
    }
}


