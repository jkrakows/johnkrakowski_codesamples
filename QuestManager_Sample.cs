﻿/*
 * AddQuest(), AddNextAchievement(), RemoveQuest(), and TurnInQuest() are integral 
 * to the in-game quest system and work in tandem with the event system. In-game 
 * achievements utilize the Quest class because achievements and quests function similarly.
 */


/// <summary>
/// Attempts to add Quest q based on QuestStatus and QuestType. If successful, q is 
/// subscribed to the event, UpdateProgress.
/// </summary>
public void AddQuest(Quest q)
    {
        switch (q.questStatus)
        {
            case QuestStatus.NotEligible:
                Debug.Log("Quest: " + q.questDetails.Title + " is " + q.questStatus);
                return;
            case QuestStatus.Eligible:
                q.questStatus = QuestStatus.Accepted;
                q.ResetObjectives();
                break;
        }

        switch (q.qID.QType)
        {
            case QuestType.Achievement:
                if (!currentAchievementQuestsList.Exists(x => x.qID == q.qID))
                {
                    if (currentAchievementQuestsList.Count > q.qID.QuestID)
                        { currentAchievementQuestsList.Insert(q.qID.QuestID, q); }
                    else
                        { currentAchievementQuestsList.Add(q); }
                }
                else
                    { return; /*Prevent Subscription if already added*/ }

                break;
            case QuestType.AdventureModeWinQuest:
                adventureModeWinQuest = q;
                break;
            default:
                Debug.Log("Error, Quest: " + q.questDetails.Title + " has invalid QuestType.");
                return;
        }

        SubscribeUpdateProgress(q);
        Debug.Log("Successfully added Quest: " + q.questDetails.Title);
    }

    /// <summary>
    /// Removes the current achievement Quest q. 
    /// If next achievement Quest exists and is eligible, add it.
    /// </summary>
    private void AddNextAchievement(Quest q)
    {
        if (achievementDictionary[q.qID.QuestID].ContainsKey(q.questDetails.Level + 1))
        {
            Quest nextQuest = achievementDictionary[q.qID.QuestID][q.questDetails.Level + 1];
            if (nextQuest.questStatus != QuestStatus.Complete)
                { nextQuest.questStatus = QuestStatus.Eligible; }
            RemoveQuest(q);
            AddQuest(nextQuest);
        }
        else
            { q.questStatus = QuestStatus.Done; }
    }

    /// <summary>
    /// Attempts to remove Quest q. Reset status to Eligible if repeatable.
    /// If q is removed, unsubscribe from the event, UpdateProgress.
    /// </summary>
    private bool RemoveQuest(Quest q)
    {
        QuestStatus nextStatus = (q.isRepeatableForReward) ? QuestStatus.Eligible : QuestStatus.Done;
        bool isCurrentQuest = false;
        switch (q.qID.QType)
        {
            case QuestType.Achievement:
                isCurrentQuest = currentAchievementQuestsList.Remove(q);
                break;
            case QuestType.AdventureModeWinQuest:
                if (isCurrentQuest = (q == adventureModeWinQuest))
                    { adventureModeWinQuest = null; }
                break;
        }

        if (isCurrentQuest)
        {
            q.questStatus = nextStatus;
            UnsubscribeUpdateProgress(q);
            Debug.Log("Successfully removed Quest: " + q.questDetails.Title);
        }
        return isCurrentQuest;
    }

    /// <summary>
    /// Attempts to complete Quest q and receive reward. 
    /// If q is an achievement Quest, then add next achievement.
    /// </summary>
    public void TurnInQuest(Quest q, bool saveData = true)
    {
        Debug.Log("TurnInQuest");
        if (q.questStatus != QuestStatus.Complete)
        {
            Debug.Log("Error, Quest: " + q.questDetails.Title + " is marked as " + q.questStatus);
            return;
        }
        q.ReceiveReward();
        
        if (q.qID.QType == QuestType.Achievement)
            { AddNextAchievement(q); }
        else if (!RemoveQuest(q))
            { Debug.Log("Error, Quest: " + q.questDetails.Title + " could not be removed."); }
        
        if (saveData)
            { GameManager.instance.SaveGameData(); }
    }
    //
    //***
    //
