﻿/*
 * A general purpose compilation of development tools, here are a few
 * examples of the tools I used. To use a tool, simply call TOOLS.ToolFunctionName()
 */


public abstract class TOOLS
{
    /// <summary>
    /// Get the next element in the list, or return the first element if last index is given.
    /// </summary>
    public static T NextOrFirst<T>(List<T> list, int index)
    {
        try
        {
            return list[index + 1];
        }
        catch (ArgumentOutOfRangeException e)
        {
            return list[0];
        }
    }

    /// <summary>
    /// Get the previous element in the list, or return the last element if the first is given.
    /// </summary>
    public static T PreviousOrLast<T>(List<T> list, int index)
    {
        try
        {
            return list[index - 1];
        }
        catch (ArgumentOutOfRangeException e)
        {
            return list[list.Count - 1];
        }
    }

    /// <summary>
    /// Get a random key in the dictionary.
    /// </summary>
    public static TKey GetRandomDictionaryKey<TKey, TValue>(IDictionary<TKey, TValue> dict)
    {
        List<TKey> keysList = dict.Keys.ToList();
        return keysList[UnityEngine.Random.Range(0, keysList.Count)];
    }

    /// <summary>
    /// A coroutine function that performs a user-defined action after a user-defined condition is met.
    /// </summary>
    public static IEnumerator PerformActionAfterCondition(Func<bool> condition, Action action)
    {
        yield return new WaitUntil(condition);
        action();
    }

    /// <summary>
    /// A variant of the Unity API function "WaitForSeconds(float time)", useful when the game is paused.
    /// </summary>
    public static IEnumerator WaitForUnscaledSeconds(float time)
    {
        float start = Time.realtimeSinceStartup;
        while (Time.realtimeSinceStartup < start + time)
            { yield return null; }
    }

    /// <summary>
    /// Is the GameObject directly in front of a cliff?
    /// </summary>
    public static bool IsFacingCliff(GameObject go)
    {
        int layerMask = (1 << LayerMask.NameToLayer(GAMECONSTANTS.LAYERNAME_CLIMB));
        float distToGround = go.GetComponent<EnvironmentObject>().idleBehaviorDetails.centerPointRadius + 0.25f;
        
        return !(Physics.Raycast(go.transform.position + go.transform.forward, -go.transform.up, distToGround, layerMask));
    }

    /// <summary>
    /// Rotate point around pivot by angles amount.
    /// </summary>
    public static Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Vector3 angles)
    {
        return Quaternion.Euler(angles) * (point - pivot) + pivot;
    }

    /// <summary>
    /// Get the distance between two points, Vector3 origin and Vector3 destination.
    /// </summary>
    public static float GetDistanceBetweenPoints(Vector3 origin, Vector3 destination)
    {
        float distance = (destination - origin).magnitude;
        return distance;
    }

    /// <summary>
    /// Get the direction between two points, Vector3 origin and Vector3 destination.
    /// </summary>
    public static Vector3 GetDirectionBetweenPoints(Vector3 origin, Vector3 destination)
    {
        Vector3 direction = (destination - origin).normalized;
        return direction;
    }

    /// <summary>
    /// Recursively obtain the last player tail object.
    /// </summary>
    public static GameObject GetLinkedObjectTail(GameObject linkedGO)
    {
        ILinkedObject linkedObj = linkedGO.GetComponent<ILinkedObject>();
        if (linkedObj.Next == null)
            { return linkedGO; }
        return GetLinkedObjectTail(linkedObj.Next.gameObject);
    }
}
